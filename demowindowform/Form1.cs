﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Pipes;

namespace demowindowform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var message = textBox1.Text;
            var namedPipeServer = new NamedPipeServerStream("pipe-named", PipeDirection.InOut, 1, PipeTransmissionMode.Byte);
            var streamReader = new StreamReader(namedPipeServer);
            namedPipeServer.WaitForConnection();
            var writer = new StreamWriter(namedPipeServer);
            writer.Write(message);
            writer.Write((char)0);
            writer.Flush();
            namedPipeServer.WaitForPipeDrain();
            textBox2.Text = streamReader.ReadLine();
            namedPipeServer.Dispose();

        }

    }
}
