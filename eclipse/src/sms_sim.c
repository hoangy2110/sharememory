/******************************************************************************
* M1 Communication Inc.
* (c) Copyright 2014 M1 Communication Inc.
* ALL RIGHTS RESERVED.
***************************************************************************/
/*!
 * \file      sms_sim.c
 * \author    Hoang Y
 * \date      August-09-2021
 */

#include "sms_sim.h"

/******************************************************************************
* Global functions
******************************************************************************/

/*!
 * ReadString function
 * \brief read string from fileHandle
 * ReadFile()
 * \return string read
 */
void ReadString(char* output) {
  ULONG read = 0;
  int32_t index = 0;
  do
  {
	  ReadFile(fileHandle, output + index++, 1, &read, NULL);
  }
  while (read > 0 && *(output + index - 1) != 0);
}

/*!
 * sms_isNewSMS function
 * \brief Notification of new SMS received
 * print notification
 */
void sms_isNewSMS(int32_t leng_SMS)
{
	if (leng_SMS == 0)
	printf("Khong co tin nhan!\n");
	else
	printf("Co tin nhan moi!\n");
}

/*!
 * sms_ReadData function
 * \brief read data from input string
 */
void sms_ReadData(char* buffer, int32_t leng_SMS)
{
	if (leng_SMS != 0)
	{
		printf("read data from server: ");
		int32_t i;
		for(i = 0; i < leng_SMS; i++)
		{
			printf("%c",buffer[i]);
		}
	}
}

/*!
 * sms_WtrieData function
 * \brief write data to file and send
 * WriteFile()
 */
void sms_WriteData(const char* contentMessage)
	{
		DWORD numWritten;
		WriteFile(fileHandle,
				contentMessage,
				strlen(contentMessage),
				&numWritten,
				NULL);
	}

/*!
 * Main function
 * Ref CreateFile(),strset(),ReadString(),sms_ReadData(),sms_WriteData()
 * Ref sms_isNewSMS()
 */
int main()
{
  // create file
	fileHandle = CreateFile(TEXT(PIPENAME),
							  GENERIC_READ | GENERIC_WRITE,
							  FILE_SHARE_WRITE,
							  NULL,
							  OPEN_EXISTING,
							  0,
							  NULL);

  // read from pipe server
  char* buffer = malloc(100);
  strset(buffer, 0);
  ReadString(buffer);

  //notification of new SMS received
  int32_t lengSMS = strlen(buffer);
  sms_isNewSMS(lengSMS);
  sms_ReadData(buffer,lengSMS);

  // send data to server
  const char* contentMsg = "hello from c++\r\n";
  sms_WriteData(contentMsg);

}
