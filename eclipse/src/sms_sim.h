/******************************************************************************
*
* M1 Communication Inc.
* (c) Copyright 2021 M1 Communication, Inc.
* ALL RIGHTS RESERVED.
*
***************************************************************************/
/*!
 * \file      sms_sim.h
 * \author    Hoang Y
 * \date      August-09-2021
 * \brief     API of module
 */

#ifndef _SMS_SIM_H_
#define _SMS_SIM_H_

/******************************************************************************
*Includes
******************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <windows.h>

/******************************************************************************
* Macros
******************************************************************************/
#define PIPENAME "\\\\.\\pipe\\pipe-named"

/******************************************************************************
* Types
******************************************************************************/
HANDLE fileHandle;

/******************************************************************************
*Global functions
******************************************************************************/
void ReadString(char* output);

void SMS_isNewSMS(int32_t leng_SMS);

void SMS_ReadData(char* buffer, int32_t leng_SMS);

void SMS_WriteData(const char* contentMessage);

#endif /* _SMS_SIM_H_ */
